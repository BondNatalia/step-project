const serviceList = document.querySelector(".services-list");
serviceList.addEventListener("click", (event) => {
    let activeBlock = document.querySelector(".active");
    activeBlock.classList.remove("active");
    event.target.classList.add("active");
    activeBlock = document.querySelector(".active");
    showContent(activeBlock);
});

function showContent(selectedService) {
    const services = document.querySelector(".services");
    const serviceBlock = services.children;
    for (const child of serviceBlock) {
        child.classList.add("invisible");

        if (child.dataset.title === selectedService.textContent) {

            child.classList.remove("invisible");
        }

    }
}

const btnLoad = document.querySelector(".btn-load");
btnLoad.addEventListener("click", load);

function load() {
    const loader = document.querySelector(".loader");
    loader.classList.remove("invisible");
    setTimeout(timeout, 3000);

    function timeout() {
        const imgInvisible = document.querySelectorAll(".work-image.invisible");
        for (img of imgInvisible) {
            img.classList.remove("invisible");
        }
        btnLoad.remove();
        loader.classList.add("invisible");
    }
}

const workList = document.querySelector(".work-list");
workList.addEventListener("click", filter);

function filter() {
    let activeBlock = document.querySelector(".active-block");
    activeBlock.classList.remove("active-block");
    event.target.classList.add("active-block");
    activeBlock = document.querySelector(".active-block");
    filtredContent(activeBlock);
}

function filtredContent(selectedWork) {
    const works = document.querySelector(".work-image-section");
    const image = works.children;
    for (const child of image) {
        child.classList.add("invisible");

        if (child.dataset.title === selectedWork.textContent) {

            child.classList.remove("invisible");
        }
        if (selectedWork.textContent === "All") {
            child.classList.remove("invisible");
        }

    }
}

const previousButton = document.querySelector(".previous");
const nextButton = document.querySelector(".next");
const previewBlock = document.querySelector(".preview");

previousButton.addEventListener("click", () => {
    let activeReview = document.querySelector(".active-review");
    activeReview.classList.remove("active-review");
    let previousReview = activeReview.previousElementSibling;
    if(previousReview) {
    previousReview.classList.add("active-review");
    activeReview = document.querySelector(".active-review");
    }
    else {
        previewBlock.lastElementChild.classList.add("active-review");
        activeReview = document.querySelector(".active-review");
    }

    showReview(activeReview);

})


nextButton.addEventListener("click", () => {
    let activeReview = document.querySelector(".active-review");
    activeReview.classList.remove("active-review");
    let nextReview = activeReview.nextElementSibling;
    if(nextReview) {
    nextReview.classList.add("active-review");
    activeReview = document.querySelector(".active-review");
    }
    else {
        previewBlock.firstElementChild.classList.add("active-review");
        activeReview = document.querySelector(".active-review");
    }
    showReview(activeReview);

})


previewBlock.addEventListener("click", (event) => {
    let activeReview = document.querySelector(".active-review");
    activeReview.classList.remove("active-review");
    event.target.classList.add("active-review");
    activeReview = document.querySelector(".active-review");
    showReview(activeReview);
})

function showReview(selectedReview) {
    const reviews = document.querySelector(".review-list");
    const reviewBlock = reviews.children;
    for (const child of reviewBlock) {
        child.classList.add("invisible");

        if (child.dataset.name === selectedReview.dataset.name) {

            child.classList.remove("invisible");
        }

    }
}
